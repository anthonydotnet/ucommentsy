﻿using Umbraco.Core.Models;
using Umbraco.Web;


namespace uCommentsy.BusinessLogic.Helpers
{
    public class CommentHelper
    {
        /// <summary>
        /// Creates a #! link for the given comment node.
        /// </summary>
        /// <returns></returns>
        public static string GetCommentUrl(IPublishedContent d, bool complete)
        {
            string hash = d.Url().Replace(d.Parent.Parent.Url(), string.Empty);
            if (complete)
            {
                return d.Parent.Parent.Url() + "#!/" + hash;
            }

            return "#!/" + hash;
        }



        public static string GetUnsubscribeUrl(string postUrl, string guid)
        {
            return string.Format("{0}?action=unsubscribe&guid={1}", postUrl, guid);
        }
    }
}
