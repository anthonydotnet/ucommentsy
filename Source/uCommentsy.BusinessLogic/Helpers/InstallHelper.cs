﻿namespace uCommentsy.BusinessLogic.Helpers
{
    using System;
    using System.Configuration;
    using System.Web;
    using System.Web.Configuration;
    using System.Linq;

    using Umbraco.Core;
    using Umbraco.Core.Models;

    using uCommentsy.BusinessLogic.Constants;

    using uHelpsy.Extensions;
    using umbraco.BusinessLogic;
    using MemberType = umbraco.cms.businesslogic.member.MemberType;

    public class InstallHelper
    {
        /// <summary>
        /// Inserts recaptcha keys
        /// </summary>
        public void InsertReCaptcha()
        {
            // insert recaptcha keys
            var config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
            config.AppSettings.Settings.Remove("domain");

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["RecaptchaPublicKey"]))
            {
                config.AppSettings.Settings.Add(new KeyValueConfigurationElement("RecaptchaPublicKey", string.Empty));
            }

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["RecaptchaPrivateKey"]))
            {
                config.AppSettings.Settings.Add(new KeyValueConfigurationElement("RecaptchaPrivateKey", string.Empty));
            }

            // restart app
            config.Save();
        }



        /// <summary>
        /// Installs properties and member.
        /// </summary>
        public void Install()
        {
            try
            {
                var contentTypes = ApplicationContext.Current.Services.ContentTypeService.GetAllContentTypes();
                foreach (var alias in ConfigReader.Instance.GetTargetDocumentTypeAliases())
                {
                    var contentType = contentTypes.SingleOrDefault(x => x.Alias == alias);

                    if (contentType != null)
                    {
                        // add property
                        this.InsertCommentDisableProperty(contentType);

                        // add allowed children 
                        this.InsertAllowedChildren(contentType, Keys.uCommentsyContainerComment);
                    }
                }

                AllowConfigurationOnRoot();

                // add member
    //            this.CreateMemberTypeAll(ConfigReader.Instance.GetMemberTypeAlias(), ConfigReader.Instance.GetMemberTypeAlias());

                // disable install on app start
                ConfigReader.Instance.ToggleInstallOnAppStart(false);
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Custom, -1, "Error installing uCommentsy." + ex);
            }
        }




        private void AllowConfigurationOnRoot()
        {
            var contentTypes = ApplicationContext.Current.Services.ContentTypeService.GetAllContentTypes();
            var contentType = contentTypes.SingleOrDefault(x => x.Alias == ConfigReader.Instance.GetRootDocTypeAlias());

            if (contentType != null)
            {
                // add allowed children 
                this.InsertAllowedChildren(contentType, Keys.uCommentsyConfiguration);
            }
        }




        /// <summary>
        /// Installs disable comments property.
        /// </summary>
        /// <param name="contentType"> </param>
        protected void InsertCommentDisableProperty(IContentType contentType)
        {
            // TODO: if not exists, add
        }




        /// <summary>
        /// Installs allowed children
        /// </summary>
        /// <param name="contentType"> </param>
        protected void InsertAllowedChildren(IContentType contentType, string childAlias)
        {
            if (contentType.AllowedContentTypes.Any(x => x.Alias == childAlias))
            {
                return;
            }

            var child = ApplicationContext.Current.Services.ContentTypeService.GetAllContentTypes().SingleOrDefault(x => x.Alias == childAlias);

            if (child == null)
            {
                return;
            }

            contentType.AddContentType(child);
            ApplicationContext.Current.Services.ContentTypeService.Save(contentType);
        }





        /// <summary>
        /// Creates member type.
        /// </summary>
        /// <param name="alias"></param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public MemberType CreateMemberTypeAll(string alias, string typeName)
        {
            // get member type or create
            var memberType = MemberType.GetByAlias(alias) ?? MemberType.MakeNew(new User(0), alias);

            if (string.IsNullOrEmpty(memberType.Text))
            {
                memberType.Text = typeName;
            }

            // get or create subscriptions tab
            var tab = memberType.getVirtualTabs.SingleOrDefault(x => x.Caption == "Subscriptions");
            int subscriptionsTabId = tab == null ? memberType.CreateTab("Subscriptions") : tab.Id;

            // add property to subscriptions tab
            memberType = AddProperty(memberType, subscriptionsTabId, "Textbox multiple", "uCommentsyMemberSubscriptions", "Subscriptions", "A newLine delimited list in the form  postId|unsubscribeGuid|dateCreated");

            // create the info tab
            int infoTabId = memberType.CreateTab("Subscriber Info");
            memberType.Save();

            // Add the properties to info tab
            memberType = AddProperty(memberType, infoTabId, "Textstring", "uCommentsyMemberName", "Name", "The name of the subscriber");
            memberType = AddProperty(memberType, infoTabId, "Textstring", "uCommentsyMemberWebsite", "Website", string.Empty);
            memberType = AddProperty(memberType, infoTabId, "Textstring", "uCommentsyMemberCommentUrlName", "Name for Comment Url", "Used to distinguish members with the same name");
            memberType.Save();

            return memberType;
        }



        /// <summary>
        /// Adds property if not exists.
        /// </summary>
        /// <param name="memberType"></param>
        /// <param name="tabId"></param>
        /// <param name="type"></param>
        /// <param name="alias"></param>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        /// <returns></returns>
        MemberType AddProperty(MemberType memberType, int tabId, string type, string alias, string name, string desc)
        {
            if (memberType.getPropertyType(alias) != null)
            {
                return memberType;
            }

            memberType.CreateProperty(tabId, type, alias, name, desc, false);
            memberType.Save();
            return memberType;
        }
    }
}