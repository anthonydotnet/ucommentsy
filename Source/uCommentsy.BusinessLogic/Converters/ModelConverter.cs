﻿using System;
using uCommentsy.BusinessLogic.Models;
using Umbraco.Core.Models;

namespace uCommentsy.BusinessLogic.Converters
{
    public static class ModelConverter
    {
        public static Subscription ToSubscrptionData(this CommentInfo info)
        {
            var model = new Subscription();
            model.Email = info.Email;
            model.Name = info.Name;
            model.Created = info.Created;
            return model;
        }


    }
}
