﻿using System.Collections.Generic;
using System.Linq;
using uCommentsy.BusinessLogic.Factories;
using uCommentsy.BusinessLogic.Models;
using uCommentsy.BusinessLogic.Services;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace uCommentsy.BusinessLogic.Repositories
{
    public interface ISubscriptionRepository
    {
        void AddSubscription(IPublishedContent post, Subscription subscription);
        void RemoveSubscription(IPublishedContent post, string email);

        bool HasSubscription(IPublishedContent post, string email);
        bool HasValidSubscriptionGuid(IPublishedContent post, string guid);

        List<Subscription> GetSubscriptions(IPublishedContent post);

        Subscription GetSubscription(IPublishedContent post, string guid);
    }



    public class SubscriptionRepository : ISubscriptionRepository
    {
        private readonly IUmbracoService _umbracoService;
        private readonly IContentService _contentService;

        public SubscriptionRepository(IUmbracoService umbracoService, IContentService contentService)
        {
            _umbracoService = umbracoService;
            _contentService = contentService;
        }


        /// <summary>
        /// 
        /// </summary>
        public void AddSubscription(IPublishedContent post, Subscription subscription)
        {
            // get comment folder using content service - hitting db directly
            var commentContainer = _umbracoService.GetCommentContainer(post);

            // get current subscriptions
            var subscriptions = GetSubscriptions(post);

            // add 
            subscriptions.Add(subscription);

            var subscriptionString = string.Join("\n", subscriptions);

            // now update the node
            commentContainer.SetValue("uCommentsySubscriptionsData", subscriptionString);
            _contentService.Save(commentContainer);
            _contentService.Publish(commentContainer);
        }



        /// <summary>
        /// 
        /// </summary>
        public void RemoveSubscription(IPublishedContent post, string guid)
        {
            // get comment folder using content service - hitting db directly
            var commentContainer = _umbracoService.GetCommentContainer(post);

            // get current subscriptions
            var subscriptions = GetSubscriptions(post);

            // remove subscription 
            subscriptions = subscriptions.Where(x => x.Guid != guid).ToList();

            var subscriptionString = string.Join("\n", subscriptions);

            // now update the node
            commentContainer.SetValue("uCommentsySubscriptionsData", subscriptionString);
            _contentService.Save(commentContainer);
            _contentService.Publish(commentContainer);
        }




        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool HasSubscription(IPublishedContent post, string email)
        {
            // get subscriptions
            var subscriptions = GetSubscriptions(post);

            return subscriptions.Any(x => x.Email == email);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool HasValidSubscriptionGuid(IPublishedContent post, string guid)
        {
            // get subscription
            var subscription = GetSubscription(post, guid);

            // return true if has subscription
            return subscription != null;
        }



        /// <summary>
        /// Get subscriptions for current node.
        /// </summary>
        /// <returns></returns>
        public List<Subscription> GetSubscriptions(IPublishedContent post)
        {
            var container = _umbracoService.GetCommentContainer(post);

            var subscriptionString = container.GetValue<string>("uCommentsySubscriptionsData") ?? string.Empty;

            // get subscriptions
            var subscriptions = ServiceFactory.CreateModelFactory().CreateSubscriptionDatas(subscriptionString);

            return subscriptions;
        }



        /// <summary>
        /// Gets subscription by unsubscribe guid.
        /// </summary>
        /// <returns></returns>
        public Subscription GetSubscription(IPublishedContent post, string guid)
        {
            // get subscriptions
            var subscriptions = GetSubscriptions(post);

            return subscriptions.FirstOrDefault(x => x.Guid == guid); // i used FirstOrDefault in case someone screws up the guid.
        }
    }
}
