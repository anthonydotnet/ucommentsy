﻿using System.Linq;
using uCommentsy.BusinessLogic.Models;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace uCommentsy.BusinessLogic.Repositories
{
    public interface ICommentRepository
    {
       
        IContent CreateComment(IPublishedContent userHostAddress, CommentInfo commentInfo, bool autoApprove);
    }

    public class CommentRepository : ICommentRepository
    {
        private readonly IContentService _contentService;


        public CommentRepository(IContentService contentService)
        {
            _contentService = contentService;
        }


        /// <summary>
        /// Creates an umbraco node under the current page
        /// </summary>
        public IContent CreateComment(IPublishedContent node, CommentInfo commentInfo, bool autoApprove)
        {
            // get comments folder
            var commentsFolder = EnsureCommentsFolder(node);

            // create content node
            var comment  = _contentService.CreateContent("Comment", commentsFolder.Id, "uCommentsyComment");

            comment.SetValue("uCommentsyCommentName", commentInfo.Name);
            comment.SetValue("uCommentsyCommentEmail", commentInfo.Email);
            comment.SetValue("uCommentsyCommentWebsite", commentInfo.Website);
            comment.SetValue("uCommentsyCommentMessage", commentInfo.Message);
            comment.SetValue("uCommentsyCommentPublicDomain", commentInfo.PublicUrl);
            comment.SetValue("uCommentsyCommentDate", commentInfo.Created);
            _contentService.Save(comment);

            if (autoApprove)
            {
                _contentService.Publish(comment);
            }
            return comment;
        }




        /// <summary>
        /// Ensures that a comment container exists under the current post
        /// </summary>
        protected IContent EnsureCommentsFolder(IPublishedContent node)
        {
            // use the content service (hitting db directly)
            var contentService = UmbracoContext.Current.Application.Services.ContentService;

            var commentsFolder = contentService.GetChildren(node.Id).FirstOrDefault(x => x.ContentType.Alias == "uCommentsyContainerComment");

            //commentsFolder = IContentHelper.EnsureNodeExists(node.Id, commentsFolder, "uCommentsyContainerComment", "Comments", true);

            if (commentsFolder == null)
            {
                commentsFolder = _contentService.CreateContent("Comments", node.Id, "uCommentsyContainerComment");
                _contentService.Save(commentsFolder);
            }

            // publish comments folder when not published
            if (!commentsFolder.Published)
            {
                contentService.Publish(commentsFolder);
            }

            return commentsFolder;
        }
    }
}
