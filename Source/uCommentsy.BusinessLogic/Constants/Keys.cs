﻿namespace uCommentsy.BusinessLogic.Constants
{
    /// <summary>
    /// Constants
    /// </summary>
    public class Keys
    {
        public static string uCommentsyArticleDisableComments = "uCommentsyArticleDisableComments";
        public static string uCommentsySpamDisableComments = "uCommentsySpamDisableComments";

        public static string RecaptchaPublicKey = "RecaptchaPublicKey";
        public static string RecaptchaPrivateKey = "RecaptchaPrivateKey";

        public static string uCommentsyContainerComment = "uCommentsyContainerComment";

        public static string uCommentsyConfiguration = "uCommentsyConfiguration";

    }
}
