﻿namespace uCommentsy.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Hosting;
    using System.Xml.Linq;

    public class ConfigReader
    {
        private const string _ConfigPath = "~/config/uCommentsy.config";

         #region Singleton

        protected XDocument m_Doc;
        protected static volatile ConfigReader m_Instance = new ConfigReader();
        protected static object syncRoot = new Object();

        protected ConfigReader()
        {
            m_Doc = XDocument.Parse(File.ReadAllText(HostingEnvironment.MapPath(_ConfigPath)));
        }

        public static ConfigReader Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    lock (syncRoot)
                    {
                        if (m_Instance == null)
                        {
                            m_Instance = new ConfigReader();
                        }
                    }
                }

                return m_Instance;
            }
        }



        #endregion

        /// <summary>
        /// Get InstallOnStartup
        /// </summary>
        /// <returns></returns>
        public bool GetInstallOnStartup()
        {
            return bool.Parse(this.m_Doc.Descendants("InstallOnStartup").Single().Value);
        }


        ///// <summary>
        ///// Get MemberTypeAlias
        ///// </summary>
        ///// <returns></returns>
        //public string GetMemberTypeAlias()
        //{
        //    return this.m_Doc.Descendants("MemberTypeAlias").Single().Value;
        //}


        ///// <summary>
        ///// Gets EmailPrefix 
        ///// </summary>
        ///// <returns></returns>
        //public string GetEmailPrefix()
        //{
        //    return this.m_Doc.Descendants("EmailPrefix").Single().Value;
        //}


        /// <summary>
        /// Gets RootDocTypeAlias 
        /// </summary>
        /// <returns></returns>
        public string GetRootDocTypeAlias()
        {
            return this.m_Doc.Descendants("RootDocTypeAlias").Single().Value;
        }


        /// <summary>
        /// Gets TargetDocumentTypeAliases 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetTargetDocumentTypeAliases()
        {
            return this.m_Doc.Descendants("TargetDocumentTypeAliases").Single().Value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        }


        /// <summary>
        /// Gets true or false from sendNotificationOnRepublish
        /// </summary>
        /// <returns></returns>
        public bool GetSendNotificationOnRepublish()
        {
            var comments = this.m_Doc.Descendants("Comments").Single();

            var sendNotificationOnRepublish = comments.Descendants("SendNotificationOnRepublish").Single().Value;

            return bool.Parse(sendNotificationOnRepublish);
        }




        ///// <summary>
        ///// Replaces member alias
        ///// </summary>
        ///// <param name="alias">Alias to insert.</param>
        //public void ChangeMemberAlias(string alias)
        //{
        //    var memberTypeAliasElem = m_Doc.Descendants("MemberTypeAlias").Single();
        //    memberTypeAliasElem.Value = alias;
        //    File.WriteAllText(HostingEnvironment.MapPath(_ConfigPath), m_Doc.ToString());
        //}




        /// <summary>
        /// Replaces member alias
        /// </summary>
        public void ToggleInstallOnAppStart(bool start)
        {
            var installOnStartupElem = this.m_Doc.Descendants("InstallOnStartup").Single();
            installOnStartupElem.Value = start.ToString().ToLower();
            File.WriteAllText(HostingEnvironment.MapPath(_ConfigPath), m_Doc.ToString());
        }



        /// <summary>
        /// Adds target aliases
        /// </summary>
        /// <param name="csv"></param>
        public void AddTargetAliases(string csv)
        {
            var newAliases = csv.Trim().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim());

            var aliases = this.GetTargetDocumentTypeAliases().ToList();
            aliases.AddRange(newAliases);

            var targetElem = this.m_Doc.Descendants("TargetDocumentTypeAliases").Single();
            targetElem.Value = string.Join(",", aliases);
            File.WriteAllText(HostingEnvironment.MapPath(_ConfigPath), m_Doc.ToString());
        }
    }
}
