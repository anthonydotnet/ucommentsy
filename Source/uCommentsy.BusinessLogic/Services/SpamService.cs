﻿using Joel.Net;
using Umbraco.Core.Models;

namespace uCommentsy.BusinessLogic.Services
{
    public interface ISpamService
    {
        bool IsSpam(string userHostAddress, string urlHost, string userAgent, IPublishedContent node, string name,
            string email, string message, string commentAuthorUrl);

        bool AutoDeleteSpam(IPublishedContent node);

        bool GetAutoApprove(IPublishedContent node);
    }

    public class SpamService : ISpamService
    {
        private readonly IUmbracoService _umbracoService;
        public SpamService(IUmbracoService umbracoService)
        {
            _umbracoService = umbracoService;
        }

        /// <summary>
        /// Uses Akismet service to detect spam
        /// </summary>
        /// <returns></returns>
        public bool IsSpam(string userHostAddress, string urlHost, string userAgent, IPublishedContent node, string name, string email, string message, string commentAuthorUrl)
        {
            var configurationNode = _umbracoService.GetConfigurationNode(node); // node.AncestorOrSelf(ConfigReader.Instance.GetRootDocTypeAlias()); 

            string apiKey = configurationNode.GetProperty("uCommentsySpamAkismetAPIKey").Value.ToString();

            if (string.IsNullOrEmpty(apiKey.Trim()))
            {
                return false; // return not spam by default when no API key 
            }

            var blogUrl = urlHost;
            var api = new Akismet(apiKey, blogUrl, userAgent);
            var comment = new AkismetComment();
            comment.Blog = blogUrl;
            comment.UserIp = userHostAddress;
            comment.UserAgent = userAgent;
            comment.CommentContent = message;
            comment.CommentType = "comment";
            comment.CommentAuthorUrl = name;
            comment.CommentAuthorEmail = email;
            comment.CommentAuthorUrl = commentAuthorUrl == "http://" ? string.Empty : commentAuthorUrl;

            return api.CommentCheck(comment);
        }




        /// <summary>
        /// Returns true if we are to delete spam automatically.
        /// </summary>
        /// <returns></returns>
        public bool AutoDeleteSpam(IPublishedContent node)
        {
            var res = _umbracoService.GetValueFromConfiguration(node, "uCommentsySpamAutoDeleteSpam");
            return res == "1" || res == "True";
        }





        /// <summary>
        /// Returns true if we are to auto approve comments
        /// </summary>
        /// <returns></returns>
        public bool GetAutoApprove(IPublishedContent node)
        {
            var res = _umbracoService.GetValueFromConfiguration(node, "uCommentsySpamAutoApproveComments");
            return res == "1" || res == "True";
        }
    }
}
