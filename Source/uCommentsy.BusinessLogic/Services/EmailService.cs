﻿using System.Collections.Generic;
using System.Linq;
using uCommentsy.BusinessLogic.Helpers;
using uCommentsy.BusinessLogic.Models;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace uCommentsy.BusinessLogic.Services
{
    public interface IEmailService
    {
        void SendNotificationEmails(IPublishedContent post, CommentInfo commentInfo, string postTitle,
            List<Subscription> subscriptions);

        void SendAdminNotificationEmail(string postUrl, IPublishedContent node, CommentInfo commentInfo, int commentId);

        void SendContactEmail(IPublishedContent node, CommentInfo commentInfo);
    }



    public class EmailService : IEmailService
    {
        private readonly IUmbracoService _umbracoService;

        public EmailService(IUmbracoService umbracoService)
        {
            _umbracoService = umbracoService;
        }


        /// <summary>
        /// Sends notification emails to the subscribers of this post.
        /// </summary>
        public void SendNotificationEmails(IPublishedContent post, CommentInfo commentInfo, string postTitle, List<Subscription> subscriptions)
        {
            // get commenter's name, message etc

            // get email template node and values
            var emailProperties = _umbracoService.GetEmailProperties(post, "CommentNotification");

            // iterate over meta data of subscribers to this post
            foreach (var subscription in subscriptions)
            {
                if (subscription == null || commentInfo.Email == subscription.Email)
                {
                    // do not send notification to the commenter
                    continue;
                }

                var unsubscribeUrl = CommentHelper.GetUnsubscribeUrl(commentInfo.PublicUrl, subscription.Guid);

                // create dictionary for email
                var dictionary = new Dictionary<string, string>() { 
                        {"##PostTitle##", postTitle},
                        {"##RecipientName##", subscription.Name},
                        {"##CommentUrl##", commentInfo.PublicUrl},
                        {"##CommenterName##", commentInfo.Name},
                        {"##CommenterEmail##", commentInfo.Email},
                        {"##CommentBody##", commentInfo.Message},
                        {"##UnsubscribeUrl##", unsubscribeUrl},
                    };

                // send email to subscriber
                EmailHelper.Send(emailProperties.EmailBody, emailProperties.Subject, emailProperties.ReplyToAddress, subscription.Email, dictionary, true);
            }
        }







        /// <summary>
        /// Sends notification email to blog admin.
        /// </summary>
        public void SendAdminNotificationEmail(string postUrl, IPublishedContent node, CommentInfo commentInfo, int commentId)
        {
            var notify = _umbracoService.GetValueFromConfiguration(node, "uCommentsyContactCommentNotification");
            if (notify != "1" && notify != "True")
            {
                return;
            }

            // get email template node and values
            var emailProperties = _umbracoService.GetEmailProperties(node, "AdminCommentNotification");

            // create dictionary for email tokens
            var dictionary = new Dictionary<string, string>() { 
                        {"##CommenterName##", commentInfo.Name},
                        {"##PostTitle##", node.Name},
                        {"##CommentBody##", commentInfo.Message},
                        {"##PostUrl##", postUrl},
                        {"##CommenterEmail##", commentInfo.Email}

                        //{"##CommentUrl##", postUrl + CommentService.Instance.GetCommentUrl(IPublishedContentHelper.GetNode(commentId), false)},
                    };

            // send email to subscriber
            EmailHelper.Send(emailProperties.EmailBody, emailProperties.Subject, emailProperties.ReplyToAddress, emailProperties.AdminContactEmail, dictionary, true);
        }






        /// <summary>
        /// Sends contact email to admin
        /// </summary>
        public void SendContactEmail(IPublishedContent node, CommentInfo commentInfo)
        {
            // get email template node and values
            var emailProperties = _umbracoService.GetEmailProperties(node, "AdminContact");

            // make dictionary for tokens
            var dictionary = new Dictionary<string, string>() { 
                    {"##CommenterName##", commentInfo.Name},
                    {"##CommenterEmail##", commentInfo.Email},
                    {"##CommenterWebsite##", commentInfo.Website},
                    {"##CommentBody##", commentInfo.Message}
                };

            //EmailHelper.SendTemplated(EMAIL_TEMPLATE_PATH, "AdminContact", commentInfo.Email, adminEmail, dictionary, true);

            // send email to admin - reply-to address is admin?
            EmailHelper.Send(emailProperties.EmailBody, emailProperties.Subject, emailProperties.ReplyToAddress, emailProperties.AdminContactEmail, dictionary, true);
        }

    }



    public class EmailProperties
    {
        public string EmailBody { get; set; }
        public string Subject { get; set; }
        public string ReplyToAddress { get; set; }
        public string AdminContactEmail { get; set; }
    }
}
