﻿using System;
using System.Collections.Generic;
using System.Linq;
using Examine;
using uCommentsy.BusinessLogic.Constants;
using uCommentsy.BusinessLogic.Factories;
using uCommentsy.BusinessLogic.Models;
using uCommentsy.BusinessLogic.Repositories;
using uHelpsy.Extensions;
using Umbraco.Core.Models;
using UmbracoExamine;

namespace uCommentsy.BusinessLogic.Services
{
    public interface ICommentService
    {
        IEnumerable<SearchResult> GetComments(IPublishedContent node, bool allComments);

        Result SubmitComment(string absoluteUri, string userHostAddress, string urlHost, string userAgent, IPublishedContent node, CommentInfo commentInfo, bool subscribe);

        bool CommentsDisabled(IPublishedContent currentPage);
    }

    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        private readonly ISubscriptionService _subscriptionService;
        private readonly IUmbracoService _umbracoService;
        private readonly IEmailService _emailService;
        private readonly ISpamService _spamService;

        public CommentService(ICommentRepository commentRepository, ISubscriptionService subscriptionService, IUmbracoService umbracoService, IEmailService emailService, ISpamService spamService)
        {
            _commentRepository = commentRepository;
            _subscriptionService = subscriptionService;
            _umbracoService = umbracoService;
            _emailService = emailService;
            _spamService = spamService;
        }


        /// <summary>
        /// Gets comments for a post, or all comments.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="getAll"></param>
        /// <returns></returns>
        public IEnumerable<SearchResult> GetComments(IPublishedContent node, bool getAll)
        {
            var root = _umbracoService.GetRootNode(node);
            var searcher = ServiceFactory.GetExamineInternalSearcher();

            var criteria = searcher.CreateSearchCriteria(IndexTypes.Content);
            criteria.Field("nodeTypeAlias", "uCommentsyComment");
            criteria.Field("uCommentsySearchablePath", getAll ? root.Id.ToString() : node.Id.ToString());

            var comments = searcher.Search(criteria).ToList();

            return comments.OrderByDescending(x => x.GetValue("uCommentsyCommentDate"));
        }






        /// <summary>
        /// Creates a content node, subscribes commenter to post, and sends notifications.
        /// </summary>
        /// <returns></returns>
        public Result SubmitComment(string absoluteUri, string userHostAddress, string urlHost, string userAgent, IPublishedContent node, CommentInfo commentInfo, bool subscribe)
        {
            try
            {
                // run spam detection
                bool spam = _spamService.IsSpam(userHostAddress, urlHost, userAgent, node, commentInfo.Name, commentInfo.Email, commentInfo.Message, commentInfo.Website);
                if (spam)
                {
                    if (_spamService.AutoDeleteSpam(node))
                    {
                        // it is span AND we auto-delete it!
                        return Result.Spam;
                    }
                }
                var autoApprove = _spamService.GetAutoApprove(node);

                // create the comment node
                var comment = _commentRepository.CreateComment(node, commentInfo, autoApprove);
             
                // comment was not spam and was auto approved so now handle subscriptions
                _subscriptionService.SubscribeToPost(node, commentInfo, subscribe);

                _emailService.SendAdminNotificationEmail(absoluteUri, node, commentInfo, comment.Id);
            }
            catch (Exception ex)
            {
                Umbraco.Core.Logging.LogHelper.Error<CommentService>(string.Format("Error during comment creation for {0}", node.Id), ex);
                return Result.Error;
            }

            return Result.Success;
        }


      


        /// <summary>
        /// Returns true if checkbox selected in Umbraco.
        /// </summary>
        /// <returns></returns>
        public bool CommentsDisabled(IPublishedContent currentPage)
        {
            var disableAllComments = _umbracoService.GetValueFromConfiguration(currentPage, Keys.uCommentsySpamDisableComments);
            var disableComments = currentPage.GetProperty(Keys.uCommentsyArticleDisableComments);

            if ((disableComments != null &&
                 (disableComments.Value.ToString() == "1" || disableComments.Value.ToString() == "True")
                 || disableAllComments == "1" || disableAllComments == "True"))
            {
                return true;
            }
            return false;
        }
    }
}
