﻿using System.Linq;
using System.Web;
using uCommentsy.BusinessLogic.Constants;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace uCommentsy.BusinessLogic.Services
{
    public interface IUmbracoService
    {
        IContent GetCommentContainer(IPublishedContent post);
        IPublishedContent TypedContent(int nodeId);

        IPublishedContent GetRootNode(IPublishedContent node);
        string GetValueFromConfiguration(IPublishedContent node, string propertyAlias);
        IPublishedContent GetConfigurationNode(IPublishedContent node);

        EmailProperties GetEmailProperties(IPublishedContent node, string emailTemplateName);
    }


    public class UmbracoService : IUmbracoService
    {
        private UmbracoHelper _umbracoHelper;
        private IContentService _contentService;
        public UmbracoService()
        {
            _umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            _contentService = ApplicationContext.Current.Services.ContentService;
        }



        public IContent GetCommentContainer(IPublishedContent post)
        {
            var container = post.Children.FirstOrDefault(x => x.DocumentTypeAlias == Keys.uCommentsyContainerComment);

            if (container == null)
            {
                return null;
            }

            // get the container from the db, as the cache might be out of date.
            return _contentService.GetById(container.Id);
        }



        public IPublishedContent TypedContent(int nodeId)
        {
            return _umbracoHelper.TypedContent(nodeId);
        }




        /// <summary>
        /// Returns a value from the landing node.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="propertyAlias"></param>
        /// <returns></returns>
        public string GetValueFromConfiguration(IPublishedContent node, string propertyAlias)
        {
            return this.GetConfigurationNode(node).GetProperty(propertyAlias).Value.ToString();
        }

       


        /// <summary>
        /// Gets root node, caches result.
        /// </summary>
        /// <returns></returns>
        public IPublishedContent GetConfigurationNode(IPublishedContent node)
        {
            string cacheKey = "uCommentsy_GetConfigurationNode";

            var cached = HttpContext.Current.Items[cacheKey] as IPublishedContent;
            if (cached != null)
            {
                return cached;
            }

            // get ancestor with alias, or get level 1.
            var n = node.AncestorOrSelf(ConfigReader.Instance.GetRootDocTypeAlias()) ?? node.AncestorOrSelf(1);

            // TODO: do BFS to get configuration node using uhelpsy
            n = n.Descendants("uCommentsyConfiguration").Single();

            // cache the result
            HttpContext.Current.Items.Add(cacheKey, n);

            return n;
        }



        /// <summary>
        /// Gets root node, caches result.
        /// </summary>
        /// <returns></returns>
        public IPublishedContent GetRootNode(IPublishedContent node)
        {
            string cacheKey = "uCommentsy_GetRootNode";

            var cached = HttpContext.Current.Items[cacheKey] as IPublishedContent;
            if (cached != null)
            {
                return cached;
            }

            // get ancestor with alias, or get level 1.
            var n = node.AncestorOrSelf(ConfigReader.Instance.GetRootDocTypeAlias()) ?? node.AncestorOrSelf(1);

            // cache the result
            HttpContext.Current.Items.Add(cacheKey, n);

            return n;
        }





        /// <summary>
        /// Gets values from email template
        /// 
        /// I dont know if this is good or bad but it makes the other methods more concise :|
        /// </summary>
        public EmailProperties GetEmailProperties(IPublishedContent node, string emailTemplateName)
        {
            // get email tempalte node from Umbraco
            var templateNode = GetEmailTemplateNode(node, emailTemplateName);

            var emailProps = new EmailProperties();

            emailProps.EmailBody = templateNode.GetProperty("uCommentsyEmailBody").Value.ToString();
            emailProps.Subject = templateNode.GetProperty("uCommentsyEmailSubject").Value.ToString();

            emailProps.ReplyToAddress = templateNode.GetProperty("uCommentsyEmailReplyToOverride").Value.ToString() != string.Empty
                                       ? templateNode.GetProperty("uCommentsyEmailReplyToOverride").Value.ToString()
                                       : GetValueFromConfiguration(node, "uCommentsyContactReplyToEmail"); // overrides replyToAddress if exists

            emailProps.AdminContactEmail = templateNode.GetProperty("uCommentsyEmailContactEmailOverride").Value.ToString() != string.Empty
                                       ? templateNode.GetProperty("uCommentsyEmailContactEmailOverride").Value.ToString()
                                       : GetValueFromConfiguration(node, "uCommentsyContactEmail"); // overrides replyToAddress if exists
            return emailProps;

        }




        /// <summary>
        /// Gets email template node in current tree with ConfigReader.Instance.GetRootDocTypeAlias() as root.
        /// </summary>
        /// <returns></returns>
        protected IPublishedContent GetEmailTemplateNode(IPublishedContent node, string templateName)
        {
            // get root
            var configurationNode = GetConfigurationNode(node);

            // get node
            var templateNode = configurationNode
                                .Descendants()
                                .Single(x => x.GetPropertyValue<string>("uCommentsyEmailTemplateName") == templateName);

            return templateNode;
        }


       
    }
}
