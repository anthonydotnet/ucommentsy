﻿using System;
using uCommentsy.BusinessLogic.Factories;
using uCommentsy.BusinessLogic.Models;
using uCommentsy.BusinessLogic.Repositories;
using umbraco.BusinessLogic;
using Umbraco.Core.Models;

namespace uCommentsy.BusinessLogic.Services
{
    public interface ISubscriptionService
    {
        void SubscribeToPost(IPublishedContent node, CommentInfo commentInfo, bool subscribe);

        Result UnsubscribeFromPost(IPublishedContent node, string guid);

        void SendSubscriptionNotifications(IContent commentNode);
    }



    public class SubscriptionService : ISubscriptionService
    {
        private readonly ISubscriptionRepository _subscriptionRepository;
        private readonly IUmbracoService _umbracoService;
        private readonly IEmailService _emailService;

        public SubscriptionService(  ISubscriptionRepository subscriptionRepository, IUmbracoService umbracoService, IEmailService emailService)
        {
            _subscriptionRepository = subscriptionRepository;
            _umbracoService = umbracoService;
            _emailService = emailService;
        }


        /// <summary>
        /// 
        /// </summary>
        public void SubscribeToPost(IPublishedContent post, CommentInfo commentInfo, bool subscribe)
        {
            if (subscribe == false)
            {
                return;
            }

            var hasSubscription = _subscriptionRepository.HasSubscription(post, commentInfo.Email);           

            // add subscription
            if (hasSubscription == false)
            {
                var subscription = ServiceFactory.CreateModelFactory().CreateSubscription(commentInfo);
                _subscriptionRepository.AddSubscription(post, subscription);               
            }
        }




        /// <summary>
        /// Sends notifications
        /// </summary>
        /// <param name="commentNode"></param>
        public void SendSubscriptionNotifications(IContent commentNode)
        {
            // this is all a bit much just to get nodes right?
            var container = _umbracoService.TypedContent(commentNode.ParentId);
            var post = _umbracoService.TypedContent(container.Parent.Id);
            var commentInfo = ServiceFactory.CreateModelFactory().CreateCommentInfoFromComment(commentNode);
            var subscriptions = _subscriptionRepository.GetSubscriptions(post);

            // send out notifications
            _emailService.SendNotificationEmails(post, commentInfo, post.Name, subscriptions);
        }


     


        /// <summary>
        /// Removes member from comments meta data, and removes post from member's subscriptions.
        /// </summary>
        /// <returns></returns>
        public Result UnsubscribeFromPost(IPublishedContent post, string guid)
        {
            try
            {
                // check if member is subscribed
                var hasSubscription = _subscriptionRepository.HasValidSubscriptionGuid(post, guid);
                if (hasSubscription ==false)
                {
                    return Result.SubscriptionNotFound;
                }

                _subscriptionRepository.RemoveSubscription(post, guid);

                return Result.Success;
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Error, post.Id, ex.Message + Environment.NewLine + ex.StackTrace);
                return Result.Error;
            }
        }

    }
}
