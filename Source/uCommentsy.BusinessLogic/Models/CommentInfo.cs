﻿using System;
using System.Collections.Generic;

namespace uCommentsy.BusinessLogic.Models
{
    public class CommentInfo
    {

        public string Name { get; set; } // commenter's name
        public string Message { get; set; } // commenter's message
        public string Website { get; set; }  // commenter's website
       // public string MemberNameForUrl { get; set; }  // generated commenter url name eg. anthony-1, anthony-2 etc 
        public bool IsAuthor { get; set; }
        public string Email { get; set; }
        public string PublicUrl { get; set; } // used for multi-domain sites, and nodes that have multiple urls!
        public DateTime Created { get; set; }

    }
}
