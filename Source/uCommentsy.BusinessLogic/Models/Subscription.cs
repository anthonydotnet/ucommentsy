﻿using System;

namespace uCommentsy.BusinessLogic.Models
{
    public class Subscription
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public string Guid { get; set; }


        /// <summary>
        /// Creates a string in the from memberId|email|createdDate, 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}|{1}|{2}|{3}", Email, Name, Guid, Created);
        }
    }
}
