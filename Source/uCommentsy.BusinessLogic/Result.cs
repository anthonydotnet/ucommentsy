﻿namespace uCommentsy.BusinessLogic
{
    public enum Result
    {
        Success, 
        GuidNotFound, 
        MemberNotFound,
        SubscriptionNotFound,
        Spam,
        Error
    }
}
