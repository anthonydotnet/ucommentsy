﻿using Examine;
using uCommentsy.BusinessLogic.Repositories;
using uCommentsy.BusinessLogic.Services;
using Umbraco.Core;

namespace uCommentsy.BusinessLogic.Factories
{
    public class ServiceFactory
    {
        public static IEmailService CreateEmailService()
        {
            // TODO: cache this!
            var service = new EmailService(CreateUmbracoService());
            return service;
        }


        public static ModelFactory CreateModelFactory()
        {
            // TODO: cache this!
            var service = new ModelFactory();
            return service;
        }


        public static ISubscriptionService CreateSubscriptionService()
        {
            // TODO: cache this!
            var service = new SubscriptionService(CreateSubscriptionRepository(), CreateUmbracoService(), CreateEmailService());
            return service;
        }



        public static ICommentService CreateCommentService()
        {
            // TODO: cache this!
            var dataService = new CommentRepository(ApplicationContext.Current.Services.ContentService);
            var umbracoService = CreateUmbracoService();
            var spamService = new SpamService(umbracoService);
            var service = new CommentService(dataService, CreateSubscriptionService(), umbracoService, CreateEmailService(), spamService);
            return service;
        }


        public static IUmbracoService CreateUmbracoService()
        {
            // TODO: cache this!
            var service = new UmbracoService();
            return service;
        }

        public static ISubscriptionRepository CreateSubscriptionRepository()
        {
            var umbracoService = CreateUmbracoService();
            return new SubscriptionRepository(umbracoService, ApplicationContext.Current.Services.ContentService);
        }



        public static ISearcher GetExamineInternalSearcher()
        {
            var searcher = ExamineManager.Instance.SearchProviderCollection["InternalSearcher"];
            return searcher;
        }


    }
}
