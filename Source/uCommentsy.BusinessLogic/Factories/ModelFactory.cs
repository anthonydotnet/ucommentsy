﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uCommentsy.BusinessLogic.Models;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace uCommentsy.BusinessLogic.Factories
{
    public class ModelFactory
    {

        /// <summary>
        /// 
        /// </summary>
        public List<Subscription> CreateSubscriptionDatas(string subscriptionDataString)
        {
            // split string email|name|dateCreated
            var subscriptionStrings = subscriptionDataString.Split("\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            var subscriptions = subscriptionStrings.Select(CreateSubscriptionData).ToList();

            return subscriptions;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Subscription CreateSubscriptionData(string subscriptionDataString)
        {
            // split string email|name|dateCreated
            string[] s = subscriptionDataString.Split('|');

            var model = new Subscription();
            model.Email = s[0];
            model.Name = s[1];
            model.Guid = s[2];

            try
            {
                model.Created = DateTime.Parse(s[3]);
            }
            catch (Exception ex)
            {
                CultureInfo provider = CultureInfo.CurrentCulture;
                model.Created = DateTime.Parse(s[3], provider);
            }

            return model;
        }


        /// <summary>
        /// Used when a content node exists. Gets property values from IPublishedContent.
        /// </summary>
        public CommentInfo CreateCommentInfoFromComment(IContent commentNode)
        {
            var name = commentNode.GetValue<string>("uCommentsyCommentName");
            var message = commentNode.GetValue<string>("uCommentsyCommentMessage");
            var email = commentNode.GetValue<string>("uCommentsyCommentEmail");
            var website = commentNode.GetValue<string>("uCommentsyCommentWebsite");
            var publicDomain = commentNode.GetValue<string>("uCommentsyCommentPublicDomain");

            var created = !string.IsNullOrEmpty(commentNode.GetValue<string>("uCommentsyCommentDate"))
                              ? commentNode.GetValue<DateTime>("uCommentsyCommentDate")
                              : commentNode.CreateDate;

            var ci = CreateCommentInfo(name, email, website, message, false, publicDomain, created);

            return ci;
        }


        public CommentInfo CreateCommentInfo(string name, string email, string website, string message, bool isAuthor, string publicUrl, DateTime created)
        {
            var info = new CommentInfo()
            {
                Name = name,
                Email = email,
                Message = message,
                Website = website,
                IsAuthor = isAuthor,
                PublicUrl = publicUrl,
                Created = created,
            };

            //info.MemberNameForUrl = CreateNameForContentNode(info., name);

            return info;
        }



        public Subscription CreateSubscription(CommentInfo commentInfo)
        {
            var model = new Subscription();
            model.Email = commentInfo.Email;
            model.Name = commentInfo.Name;
            model.Created = commentInfo.Created;
            model.Guid = Guid.NewGuid().ToString();
            return model;
        }

    }
}
