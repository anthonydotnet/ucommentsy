﻿using uCommentsy.BusinessLogic.Factories;
using Umbraco.Core.Logging;

namespace uCommentsy.BusinessLogic.EventHandlers
{
    using System;
    using System.Web;
    using Examine;
    using UmbracoExamine;
    using Umbraco.Core;
    using Umbraco.Core.Events;
    using Umbraco.Core.Models;
    using Umbraco.Core.Publishing;

    public class UmbracoNodeEventsForComments : IApplicationEventHandler
    {
        // used to check if emails should be sent after a publish
        private const string SkipEmailsKey = "uCommentsy.BusinessLogic.EventHandlers_SkipEmailsKey_";

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            if (applicationContext.IsConfigured && applicationContext.DatabaseContext.IsDatabaseConfigured)
            {
                //... do stuff since we are installed and configured, otherwise don't do stuff
                ExamineManager.Instance.IndexProviderCollection["InternalIndexer"].GatheringNodeData += UmbracoExamineEvents_GatheringNodeData;
                PublishingStrategy.Published += PublishingStrategy_Published;
                PublishingStrategy.Publishing += PublishingStrategy_Publishing;
            }
        }



        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }



        /// <summary>
        /// Save entity.HasPublishedVersion.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void PublishingStrategy_Publishing(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            foreach (var entity in e.PublishedEntities)
            {
                if (entity.ContentType.Alias == "uCommentsyComment")
                {
                    // flag skip emails
                    HttpContext.Current.Items[SkipEmailsKey + entity.Id] = entity.HasPublishedVersion();
                }
            }
        }






        /// <summary>
        /// Sends email notifications.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void PublishingStrategy_Published(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            foreach (var entity in e.PublishedEntities)
            {
                if (entity.ContentType.Alias != "uCommentsyComment") { return; }

                if (!ConfigReader.Instance.GetSendNotificationOnRepublish() && (bool)HttpContext.Current.Items[SkipEmailsKey + entity.Id])
                {
                    // do not send notifications if this not has already been published
                    HttpContext.Current.Items.Remove(SkipEmailsKey + entity.Id);
                    return;
                }

                try
                {
                    // this is a hack to get the node to index in umbraco 6.0.1
                    ExamineManager.Instance.ReIndexNode(entity.ToXml(), IndexTypes.Content);

                    // send out notifications
                    ServiceFactory.CreateSubscriptionService().SendSubscriptionNotifications(entity);
                }
                catch (Exception ex)
                {
                    LogHelper.Error<UmbracoNodeEventsForComments>(string.Format("Send notification failed when node {0} was published.", entity.Id), ex);
                }
            }
        }



        private void UmbracoExamineEvents_GatheringNodeData(object sender, IndexingNodeDataEventArgs e)
        {
            // index the nodes which are meant to contain comments.
            // if (ConfigReader.Instance.GetTargetDocumentTypeAliases().Contains(e.Fields["nodeTypeAlias"]))
            if (e.Fields["nodeTypeAlias"] == "uCommentsyComment")
            {
                // add path
                e.Fields.Add("uCommentsySearchablePath", e.Fields["path"].Replace(",", " "));
            }
        }
    }
}