﻿using uCommentsy.BusinessLogic.Factories;

namespace uCommentsy.Mvc.Parts.Controllers.Partials.Forms
{
    using System;
    using System.Configuration;
    using System.Web.Mvc;
    using System.Linq;
    using Microsoft.Web.Helpers;

    using BusinessLogic;
    using BusinessLogic.Constants;
    using Models.Forms;
    using Umbraco.Core.Models;
    using Umbraco.Web.Mvc;

    /// <summary>
    /// The contact form surface controller. This class was ported almost exactly from Contact.ascx.cs. It needs rewriting.
    /// </summary>
    public class uCommentsyContactFormSurfaceController : SurfaceController
    {
        public ActionResult Index(IPublishedContent currentPage)
        {
            var action = Request.QueryString["action"] ?? string.Empty;
            var success = Request.QueryString["success"] ?? string.Empty;

            if (ConfigReader.Instance.GetTargetDocumentTypeAliases().Contains(currentPage.DocumentTypeAlias))
            {
                if (action == "unsubscribe")
                {
                    if (string.IsNullOrEmpty(success))
                    {
                        DoUnsubscribe(currentPage); // does a redirect to this page after unsubscribing
                    }
                }
                else
                {
                    var commentService = ServiceFactory.CreateCommentService();

                    // check if we should disable comments 

                    var disabled = commentService.CommentsDisabled(currentPage);
                    if (disabled)
                    {
                        AddTempData("closed", true);
                    }
                    else
                    {
                        AddTempData("closed", false);
                    }
                }
            }
            else
            {
                // this is a bit hacky... we use the form for posts and contact..temp data will cause crash otherwise
                AddTempData("closed", false);
            }
            ReCaptcha.PublicKey = ConfigurationManager.AppSettings[Keys.RecaptchaPublicKey];

            return PartialView("/Views/Partials/uCommentsy/Forms/uCommentsyFormContact.cshtml", new ContactFormModel());
        }




        /// <summary>
        /// The post.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Post(ContactFormModel model)
        {
            // model not valid, do not save, but return current umbraco page
            if (!ModelState.IsValid) { return CurrentUmbracoPage(); }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings[Keys.RecaptchaPrivateKey]) && !ReCaptcha.Validate(ConfigurationManager.AppSettings[Keys.RecaptchaPrivateKey])
                && ConfigurationManager.AppSettings[Keys.RecaptchaPrivateKey] != "," && ConfigurationManager.AppSettings[Keys.RecaptchaPrivateKey] != ",")
            {
                // this allows the developer to simply remove recaptcha and the config setting if they dont want it.
                return CurrentUmbracoPage();
            }

            var currentPageUrl = Umbraco.NiceUrl(CurrentPage.Id);

            //var absoluteUri = new Uri(Request.Url.AbsoluteUri).GetLeftPart(UriPartial.Authority) + currentPageUrl;
            var absoluteUri = Request.UrlReferrer.AbsoluteUri;

            // create comment info for cleaner data passing
            var commentInfo = ServiceFactory.CreateModelFactory().CreateCommentInfo(model.Name, model.Email, model.Website, model.Message, Request.IsAuthenticated, absoluteUri, DateTime.UtcNow);


            if (ConfigReader.Instance.GetTargetDocumentTypeAliases().Contains(CurrentPage.DocumentTypeAlias))
            {
                var res = ServiceFactory.CreateCommentService().SubmitComment(Request.UrlReferrer.AbsoluteUri, Request.UserHostAddress, Request.Url.Host, Request.UserAgent, CurrentPage, commentInfo, model.Subscribe);

                if (res == Result.Spam)
                {
                    Response.Redirect(currentPageUrl + "?action=comment&success=spam#spam", true);
                    return null;
                }

                if (res == Result.Error)
                {
                    Response.Redirect(currentPageUrl + "?action=comment&success=error#error", true);
                    return null;
                }
            }
            else
            {
                ServiceFactory.CreateEmailService().SendContactEmail(CurrentPage, commentInfo);
            }

            return Redirect(currentPageUrl + "?action=comment&success=true#success");

            //Response.Redirect(currentPageUrl + "?action=comment&success=true#success", true);
        }



        /// <summary>
        /// Adds to TempData collection.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        private void AddTempData(string key, object value)
        {
            if (!TempData.ContainsKey(key)) { TempData.Add(key, value); }
        }



        /// <summary>
        /// Unsubscribes user.
        /// </summary>
        /// <param name="currentPage">
        /// The current Page.
        /// </param>
        private void DoUnsubscribe(IPublishedContent currentPage)
        {
            // first test the query strings!
            string guid = Request.QueryString["guid"];
            var currentPageUrl = Umbraco.NiceUrl(currentPage.Id);

            // do the unsubscribe"
            Result res = ServiceFactory.CreateSubscriptionService().UnsubscribeFromPost(currentPage, guid);

            if (res == Result.Success || res == Result.SubscriptionNotFound)
            {
                // redirect to current page
                Response.Redirect(currentPageUrl + "?action=unsubscribe&success=true#unsubscribed", true);
            }
            else if (res == Result.GuidNotFound || res == Result.MemberNotFound)
            {
                // redirect to current page
                Response.Redirect(currentPageUrl + "?action=unsubscribe&success=false#unsubscribed", true);
            }
            else if (res == Result.Error)
            {
                Response.Redirect(currentPageUrl + "?action=unsubscribe&success=error#unsubscribed", true);
            }
        }
    }
}