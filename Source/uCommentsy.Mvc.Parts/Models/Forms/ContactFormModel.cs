﻿namespace uCommentsy.Mvc.Parts.Models.Forms
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Mode for contact form
    /// </summary>
    public class ContactFormModel 
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$")]
        public string Email { get; set; }

        [Required]
        public string Message { get; set; }

        [DefaultValue("")]
        public string Website { get; set; }
        
        [DefaultValue(false)]
        [Required]
        public bool Subscribe { get; set; }
    }

}