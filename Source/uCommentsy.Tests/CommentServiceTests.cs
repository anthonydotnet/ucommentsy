﻿using System;
using Moq;
using NUnit.Framework;
using uCommentsy.BusinessLogic;
using uCommentsy.BusinessLogic.Constants;
using uCommentsy.BusinessLogic.Models;
using uCommentsy.BusinessLogic.Repositories;
using uCommentsy.BusinessLogic.Services;
using Umbraco.Core.Models;

namespace uCommentsy.Tests
{
    [TestFixture]
    public class CommentServiceTests
    {
        private ICommentService _commentService;
        private Mock<ICommentRepository> _commentDataServiceMock;
        private Mock<ISubscriptionService> _subscriptionServiceMock;
        private Mock<IUmbracoService> _umbracoServiceMock;
        private Mock<IEmailService> _emailServiceMock;
        private Mock<ISpamService> _spamServiceMock;

        [SetUp]
        public void SetUp()
        {
            _commentDataServiceMock = new Mock<ICommentRepository>();
            _subscriptionServiceMock = new Mock<ISubscriptionService>();
            _umbracoServiceMock = new Mock<IUmbracoService>();
            _emailServiceMock = new Mock<IEmailService>();
            _spamServiceMock = new Mock<ISpamService>();

            // TODO: the problem with constructor injection is too much injection!
            _commentService = new CommentService(_commentDataServiceMock.Object, _subscriptionServiceMock.Object,
                _umbracoServiceMock.Object, _emailServiceMock.Object, _spamServiceMock.Object);
        }


        [Test]
        public void SubmitComment_Returns_IsSpam()
        {
            var nodeMock = new Mock<IPublishedContent>();
            var commentInfo = new CommentInfo()
            {
                Name = "Anthony",
                Email = "admin@admin.com",
                Message = "message",
                Website = "website"
            };

            _spamServiceMock.Setup(
                x =>
                    x.IsSpam("hostAddress", "urlHost", "userAgent", nodeMock.Object, "Anthony", "admin@admin.com",
                        "message", "website")).Returns(true);
            _spamServiceMock.Setup(x => x.AutoDeleteSpam(nodeMock.Object)).Returns(true);

            var res = _commentService.SubmitComment("url", "hostAddress", "urlHost", "userAgent", nodeMock.Object,
                commentInfo, true);

            Assert.AreEqual(Result.Spam, res);
        }


        [Test]
        public void SubmitComment_Returns_Success()
        {
            var nodeMock = new Mock<IPublishedContent>();
            var commentInfo = new CommentInfo()
            {
                Name = "Anthony",
                Email = "admin@admin.com",
                Message = "message",
                Website = "website"
            };
            var contentMock = new Mock<IContent>();

            _commentDataServiceMock.Setup(x => x.CreateComment(nodeMock.Object, commentInfo, false))
                .Returns(contentMock.Object);

            var res = _commentService.SubmitComment("url", "hostAddress", "urlHost", "userAgent", nodeMock.Object,
                commentInfo, true);

            Assert.AreEqual(Result.Success, res);
        }


        [Test]
        public void SubmitComment_Returns_Error()
        {
            var nodeMock = new Mock<IPublishedContent>();
            var commentInfo = new CommentInfo()
            {
                Name = "Anthony",
                Email = "admin@admin.com",
                Message = "message",
                Website = "website"
            };
            var contentMock = new Mock<IContent>();

            _commentDataServiceMock.Setup(x => x.CreateComment(nodeMock.Object, commentInfo, false))
                .Throws(new Exception());

            var res = _commentService.SubmitComment("url", "hostAddress", "urlHost", "userAgent", nodeMock.Object,
                commentInfo, true);

            Assert.AreEqual(Result.Error, res);
        }


        [Test]
        public void Comments_Disabled_Returns_True_When_DisableAllComments()
        {
            //bool CommentsDisabled(IPublishedContent currentPage)
            var nodeMock = new Mock<IPublishedContent>();

            _umbracoServiceMock.Setup(x => x.GetValueFromConfiguration(nodeMock.Object, Keys.uCommentsySpamDisableComments)).Returns("1");
            var res = _commentService.CommentsDisabled(nodeMock.Object);
            Assert.True(res);

            _umbracoServiceMock.Setup(x => x.GetValueFromConfiguration(nodeMock.Object, Keys.uCommentsySpamDisableComments)).Returns("True");
            res = _commentService.CommentsDisabled(nodeMock.Object);
            Assert.True(res);

            var propertyMock = new Mock<IPublishedProperty>();
            propertyMock.Setup(x => x.Value).Returns("1");
            nodeMock.Setup(x => x.GetProperty(Keys.uCommentsyArticleDisableComments)).Returns(propertyMock.Object);
            res = _commentService.CommentsDisabled(nodeMock.Object);
            Assert.True(res);

            propertyMock.Setup(x => x.Value).Returns("True");
            res = _commentService.CommentsDisabled(nodeMock.Object);
            Assert.True(res);
        }

        [Test]
        public void Comments_Disabled_Returns_True_When_DisableAllComments_Is_False()
        {
            var nodeMock = new Mock<IPublishedContent>();

            _umbracoServiceMock.Setup(x => x.GetValueFromConfiguration(nodeMock.Object, Keys.uCommentsySpamDisableComments)).Returns("0");
            var res = _commentService.CommentsDisabled(nodeMock.Object);
            Assert.False(res);

            _umbracoServiceMock.Setup(x => x.GetValueFromConfiguration(nodeMock.Object, Keys.uCommentsySpamDisableComments)).Returns("AnythingElse");
            res = _commentService.CommentsDisabled(nodeMock.Object);
            Assert.False(res);

            var propertyMock = new Mock<IPublishedProperty>();
            propertyMock.Setup(x => x.Value).Returns("0");
            nodeMock.Setup(x => x.GetProperty(Keys.uCommentsyArticleDisableComments)).Returns(propertyMock.Object);
            res = _commentService.CommentsDisabled(nodeMock.Object);
            Assert.False(res);

            propertyMock.Setup(x => x.Value).Returns("SomethingElse");
            res = _commentService.CommentsDisabled(nodeMock.Object);
            Assert.False(res);
        }
    }
}


