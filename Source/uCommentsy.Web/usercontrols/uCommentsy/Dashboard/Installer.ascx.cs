﻿namespace uCommentsy.Web.usercontrols.uCommentsy.Dashboard
{
    using System;
    using System.Web;
    using System.Web.UI;

    using global::uCommentsy.BusinessLogic;
    using global::uCommentsy.BusinessLogic.Helpers;

    public partial class Installer : UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            try
            {
                HttpRuntime.UnloadAppDomain();
            }
            catch (Exception ex) { }
        }



        protected void Install_Click(object sender, EventArgs e)
        {
            var installHelper = new InstallHelper();

            //if (TxtMemberTypeAlias.Text.Trim() != string.Empty)
            //{
            //    ConfigReader.Instance.ChangeMemberAlias(TxtMemberTypeAlias.Text);
            //}

            if (TxtPageTypeAlias.Text.Trim() != string.Empty)
            {
                ConfigReader.Instance.AddTargetAliases(TxtPageTypeAlias.Text);
            }

            MvInstall.ActiveViewIndex = 1;

            ConfigReader.Instance.ToggleInstallOnAppStart(true);

            // causes app to restart
            installHelper.InsertReCaptcha();
        
            try
            {
                HttpRuntime.UnloadAppDomain();
            }
            catch (Exception ex){}
        }
    }
}