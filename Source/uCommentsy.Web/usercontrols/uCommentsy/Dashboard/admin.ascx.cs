﻿using System.Globalization;
using uHelpsy.Extensions;

namespace uCommentsy.Web.usercontrols.uCommentsy.dashboard
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;

    using Umbraco.Core.Models;
    using Umbraco.Core.Services;
    using Umbraco.Web;
    using Examine;
    using UmbracoExamine;

    public partial class Admin : System.Web.UI.UserControl
    {
        public List<IContent> Comments;

        public IContentService ContentService;

        public UmbracoHelper Helper;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ContentService = UmbracoContext.Current.Application.Services.ContentService;

            try
            {
                Helper = new UmbracoHelper(UmbracoContext.Current);

                if (!IsPostBack)
                {
                    var searcher = ExamineManager.Instance.SearchProviderCollection["InternalSearcher"];
                    var criteria = searcher.CreateSearchCriteria(IndexTypes.Content);
                    criteria.Field("nodeTypeAlias", "uCommentsyConfiguration");

                    var blogLandingPages = searcher.Search(criteria).ToIPublishedContent(true).Select(c => c.Parent).ToList();

                    foreach (var landingPage in blogLandingPages)
                    {
                        // add landing to ddl
                        ddlRoots.Items.Add(new ListItem(landingPage.Name, landingPage.Id.ToString(CultureInfo.InvariantCulture)));
                    }

                    ddlRoots.DataBind();
                }
            }
            catch (Exception ex)
            {
                // display errors
                string message = "Error getting results from Examine. Note: The Email Templates folder (under your desired root) must be published. You will probably need to restart your app pool the first time the uCommentsy Email Templates folder is moved. This panel is now powered by lucene, so remember indexing occurs async.";
                lblError.Text = string.Format("<br/><br/>{0}", message);
                lblErrorTrace.Text = string.Format("<br/><br/>{0}StackTrace:{1}<br/>", ex.Message, ex.StackTrace);
            }
        }




        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            LoadComments();
        }




        /// <summary>
        /// Gets unpublished comments.
        /// </summary>
        protected void LoadComments()
        {
            Comments = new List<IContent>();

            var strRoot = ddlRoots.SelectedValue;
            //var rootId = int.Parse(strRoot);
            //var root = ContentService.GetById(rootId);

            // get unpublished comments
            var searcher = ExamineManager.Instance.SearchProviderCollection["InternalSearcher"];
            var criteria = searcher.CreateSearchCriteria(IndexTypes.Content);
            criteria.Field("nodeTypeAlias", "uCommentsyComment");
            var results = searcher.Search(criteria);
            foreach (var result in results)
            {
                var contentId = result.Id;
                var content = Helper.TypedContent(contentId);
                if (content != null) { continue; }

                var path = result.Fields["path"];
                if (!string.IsNullOrWhiteSpace(path))
                {
                    var ids = path.Split(new[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);
                    if (ids.Contains(strRoot))
                    {
                        // TODO: we can make this more efficient by using the bulk method GetByIds
                        var comment = ContentService.GetById(contentId);
                        if (comment != null)
                        {
                            Comments.Add(comment);
                        }
                    }
                }
            }
        }







        /// <summary>
        /// Click event for approve.
        /// Publishes comment node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnApprove_Click(object sender, EventArgs e)
        {
            int commentId = int.Parse(uCommentsy_HdnSelectedId.Value);

            // publish node with value v
            var doc = ContentService.GetById(commentId);

            ContentService.PublishWithStatus(doc);

            // reload comments
            LoadComments();
        }






        /// <summary>
        /// Click event for delete.
        /// Deletes the comment node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            var id = int.Parse(uCommentsy_HdnSelectedId.Value);

            var doc = ContentService.GetById(id);
            ContentService.Delete(doc);

            LoadComments();
        }

        protected void btnReindex_Click(object sender, EventArgs e)
        {
            // This is a clunky workaround that allows a user without access to the developer
            // section of Umbraco to reindex Examine since it isn't always up to date.
            var indexer = ExamineManager.Instance.IndexProviderCollection["InternalIndexer"];
            indexer.RebuildIndex();
        }
    }
}