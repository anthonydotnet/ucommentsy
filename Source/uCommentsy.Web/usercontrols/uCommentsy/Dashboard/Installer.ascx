﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Installer.ascx.cs" Inherits="uCommentsy.Web.usercontrols.uCommentsy.Dashboard.Installer" %>
<div>
    <asp:MultiView ID="MvInstall" runat="server" ActiveViewIndex="0">
        <asp:View runat="server">
            <%--<asp:TextBox runat="server" ID="TxtMemberTypeAlias" placeholder="Member type alias (leave blank to use default)"
                Style="width: 300px;" />
                <br/>--%>
                
            <asp:TextBox runat="server" ID="TxtPageTypeAlias" placeholder="CSV of aliases for pages to have comments (leave blank to use default)" Style="width: 350px;" />
                <br/>
                

            <asp:Button ID="Button1" runat="server" Text="Finish Installation" OnClick="Install_Click" />
            (Your application will restart)
            
            <div>
                For more settings, see /Config/uCommentsy.config
            </div>

        </asp:View>
        <asp:View ID="View2" runat="server">
            Done!
        </asp:View>
    </asp:MultiView>
</div>
