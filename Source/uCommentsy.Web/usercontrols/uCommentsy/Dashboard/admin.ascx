﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Admin.ascx.cs" Inherits="uCommentsy.Web.usercontrols.uCommentsy.dashboard.Admin" %>
<style type="text/css">
    #uCommentsy li { list-style-type:none; border-bottom:1px solid grey; display:block; padding-top:20px; padding-bottom:20px; }
    #uCommentsy li span { display:block; }
    #uCommentsy .approve { margin-right:10px; }
    #uCommentsy .approve, #uCommentsy .delete { cursor:pointer; text-decoration:underline; }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#uCommentsy .approve').click(function () {
            $('input[id$=uCommentsy_HdnSelectedId]').val($(this).attr('rel'));
            $('input[id$=uCommentsy_btnApprove]').trigger('click');
            //$(this).parent().remove();
        });

        $('#uCommentsy .delete').click(function () {
            $('input[id$=uCommentsy_HdnSelectedId]').val($(this).attr('rel'));
            $('input[id$=uCommentsy_btnDelete]').trigger('click');
            // $(this).parent().remove();
        });
    });
</script>
<div id="uCommentsy">
    <h1>Approve comments</h1>
    <label  for="<%=ddlRoots.ClientID %>">Select site root</label>
    <asp:DropDownList ID="ddlRoots" runat="server"></asp:DropDownList>
    <br />
    <br />
    <asp:LinkButton runat="server" ID="btnReindex" OnClick="btnReindex_Click" Text="Click to reindex" ValidationGroup="uCommentsyCreatePost" /> <span>(may take a few minutes)</span>
    <br />
    <br />
    <asp:LinkButton  ID="btnSubmit" runat="server" onclick="btnSubmit_Click" Text="Click to load" ValidationGroup="uCommentsyCreatePost" />


    <% if (Comments == null)
       {
       } %>
    <% else if (Comments.Count > 0)
       { %>
    <h1>
        Comments awaiting your approval</h1>
    <%}
       else
       { %>
    <h1>
        No comments to approve</h1>
    <%} %>
    <ul>
        <%
        if (Comments != null)
        {
            foreach (var c in Comments)
            {
                var container = ContentService.GetById(c.ParentId);
                var post = ContentService.GetById(container.ParentId);
                %>
                <li><span>Article:
                    <a target="_blank" href="<%= umbraco.library.NiceUrl(post.Id) %>"><%= post.Name %></a></span>
                    <span>Date:
                        <%= c.CreateDate %></span> <span>From:
                        <%= c.GetValue<string>("uCommentsyCommentName") %></span> <span>Email:
                        <%= c.GetValue<string>("uCommentsyCommentEmail") %></span> <span>Message:
                    </span>
                    <span> <%= c.GetValue<string>("uCommentsyCommentMessage") %></span>
                    <br />
                    <a class="approve" rel="<%= c.Id %>">Approve</a> <a class="delete" rel="<%= c.Id %>">Delete</a>
                </li>
            <% }
        } %>
    </ul>
    <asp:HiddenField ID="uCommentsy_HdnSelectedId" runat="server" />
    <asp:Button Style="display: none;" ID="uCommentsy_btnApprove" Text="Approve" runat="server"
        OnClick="btnApprove_Click"  ValidationGroup="uCommentsy_Comments" />
    <asp:Button Style="display: none;" ID="uCommentsy_btnDelete" Text="Approve" runat="server" OnClick="btnDelete_Click" ValidationGroup="uCommentsy_Comments" />
</div>

<asp:Label ID="lblError" runat="server"></asp:Label>

<div style="display:none;">
    <asp:Label ID="lblErrorTrace" runat="server" ></asp:Label>
</div>

